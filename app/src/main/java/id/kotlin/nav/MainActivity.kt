package id.kotlin.nav

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.bottomnavigation.BottomNavigationView.OnNavigationItemSelectedListener
import id.kotlin.nav.features.FavoriteFragment
import id.kotlin.nav.features.HomeFragment
import id.kotlin.nav.features.SettingsFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), OnNavigationItemSelectedListener {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
    bnv_main.setOnNavigationItemSelectedListener(this)
    bnv_main.selectedItemId = R.id.navigation_home
  }

  override fun onNavigationItemSelected(menuItem: MenuItem): Boolean =
    when (menuItem.itemId) {
      R.id.navigation_home -> {
        supportFragmentManager.beginTransaction().replace(R.id.fl_main, HomeFragment()).commit()
        true
      }
      R.id.navigation_favorite -> {
        supportFragmentManager.beginTransaction().replace(R.id.fl_main, FavoriteFragment()).commit()
        true
      }
      R.id.navigation_settings -> {
        supportFragmentManager.beginTransaction().replace(R.id.fl_main, SettingsFragment()).commit()
        true
      }
      else -> false
    }
}